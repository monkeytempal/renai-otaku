<?php
    get_header();

    $review_count = get_field('review_ratings');
	$review_count = ($review_count ? $review_count : 0);
    $rating_value = $review_count*100/5;
?>

	<main class="content">
        <section class="container article-single">
        <?php get_template_part('partials/ad-leaderboard'); ?>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="article-single__header">
                    <span class="article-single__category article-single__category--primary"><?php getPrimaryCategory('article-list__category'); ?></span>
                    <h1 class="article-single__title"><?php the_title(); ?></h1>
                </header>

                <figure class="article-single__thumb">
                    <?php if ( has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail(); ?>
                    <?php endif; ?>
                
                    <!-- <figcaption class="article-single__thumb__caption"></figcaption> -->
                </figure>
                <!-- END post thumbnail -->

                <div class="row article-single__content">
                    <div class="article-single__text col-lg-7 ml-auto">
                        <div class="article-single__meta">
                            <div class="article-single__author">
                                <?php if($author_avatar = get_avatar_url(get_the_author_meta( 'ID' ), 32)): ?>
                                <img src="<?php echo $author_avatar; ?>" alt="<?php echo get_the_author_meta('display_name');?>" class="article-single__author__img">
                                <?php else: ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php echo get_the_author_meta('display_name');?>" class="article-single__author__img">
                                <?php endif; ?>
                                <p class="article-single__author__name">By <strong><?php the_author_posts_link(); ?></strong></p>
                                <p class="article-single__author__date">Published on <strong><?php the_time('F j, Y'); ?></strong></p>
                            </div>
                            <!-- END Author -->

                            <ul class="article-single__social list-inline">
                                <li class="list-inline-item">
                                    <button type="button" class="article-single__social__link" data-sharer="facebook" data-url="<?php the_permalink(); ?>"><span class="icon icon-facebook"></span></button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="button" class="article-single__social__link" data-sharer="twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>" data-hashtags="renaiotaku"><span class="icon icon-twitter"></span></button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="button" class="article-single__social__link" data-sharer="reddit" data-url="<?php the_permalink(); ?>"><span class="icon icon-reddit"></span></button>
                                </li>
                                <li class="list-inline-item">
                                    <button type="button" class="article-single__social__link" data-sharer="tumblr" data-caption="Check out this awesome article" data-title="<?php the_title(); ?>" data-tags="<?php $tags = get_the_tags(); foreach($tags as $tag) {echo "$tag->name, "; } ?>" data-url="<?php the_permalink(); ?>"><span class="icon icon-tumblr"></span></button>
                                </li>
                            </ul>
                        </div>
                        <!-- END Meta -->

                        <?php the_content(); // Dynamic Content ?>

                        <!-- Custom fields Listicle -->
                        <?php if(get_field('select_type') == 'Listicle'): ?>
                            <?php get_template_part('partials/content-listicle'); ?>
                        <?php elseif(get_field('select_type') == 'Review'): ?>
                            <?php the_field('review_content'); ?>
                            <h4 class="article-single__heading">Final Thoughts</h4>
                            <p><?php the_field('review_conclusion'); ?></p>
                        <?php else: ?>
                            <?php the_field('article_content'); ?>
                        <?php endif; 

                        $content_bottom_ad = get_field('content_bottom_ad');
                        
                        if($content_bottom_ad): ?>
                        <div class="adspace-leaderboard text-center">
                            <?php echo $content_bottom_ad ?>
                        </div>
                        <!-- END Content top Ad -->

                        <?php endif; ?>
                    </div>
                    <!-- END Article Content -->

                    <aside class="sidebar article-single__sidebar col-lg-3 mr-auto">
                        <div class="sidebar-sticky">
                            <div class="sidebar-sticky__wrapper">
                                <?php if(get_field('select_type') == 'Review'):?>
                                    <div class="sidebar-widget sidebar-review-box" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                                        <?php if($review_count): ?>
                                        <div class="sidebar-review-box__ratings">
                                            <span class="sidebar-review-box__ratings__num"><?php echo $review_count; ?></span>
                                            <span class="sidebar-review-box__ratings__text">Overall Ratings</span>
                                            <span class="sidebar-review-box__ratings__star ratings stars-outer">
                                                <span class="stars-inner" style="width: <?php echo $rating_value; ?>%;"></span>
                                            </span>
                                        </div>
                                        <?php endif; ?>
                                        
                                        <?php if(get_field('review_type')): ?>
                                            <div class="sidebar-review-box__text">
                                                <span class="sidebar-review-box__title">Info</span>
                                                <?php 
                                                    $animeReviewBox = get_field('anime_review_box');
                                                    $mangaReviewBox = get_field('manga_review_box');
                                                    $gameReviewBox = get_field('game_review_box');
                                                ?>

                                                <?php if(get_field('review_type') == 'Anime'):?>
                                                <ul class="list-unstyled">
                                                    <li><span class="sidebar-review-box__label">Episodes:</span> <?php echo $animeReviewBox['anime_episodes']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Airing Date:</span> <?php echo $animeReviewBox['anime_airing_date']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Genre:</span> <?php echo $animeReviewBox['anime_genre']; ?></li>
                                                </ul>
                                                <?php elseif(get_field('review_type') == 'Manga'):?>
                                                <ul class="list-unstyled">
                                                    <li><span class="sidebar-review-box__label">Volumes:</span> <?php echo $mangaReviewBox['manga_volumes']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Publish Date:</span> <?php echo $mangaReviewBox['manga_publish_date']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Genre:</span> <?php echo $mangaReviewBox['manga_genre']; ?></li>
                                                </ul>
                                                <?php elseif(get_field('review_type') == 'Game'):?>
                                                <ul class="list-unstyled">
                                                    <li><span class="sidebar-review-box__label">Platform:</span> <?php echo $gameReviewBox['game_platform']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Publisher:</span> <?php echo $gameReviewBox['game_publisher']; ?></li>
                                                    <li><span class="sidebar-review-box__label">Genre:</span> <?php echo $gameReviewBox['game_genre']; ?></li>
                                                </ul>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>

                                <?php
                                $recommended_article = get_field('recommended_article');

                                if( is_single() && $recommended_article ):
                                $post = $recommended_article;
                                setup_postdata( $post );
                                ?>
                                <div class="sidebar-widget">
                                    <h3 class="sidebar__title"><span>We</span> Recommend</h3>
                                    
                                    <figure class="article-recommended">
                                    <?php if(has_post_thumbnail()): ?>
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('featured', array('class' => 'article-recommended__thumbnail')); ?></a>
                                    <?php else: ?>
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/placeholder.png" alt="<?php the_title(); ?>" class="article-recommended__thumbnail"></a>
                                    <?php endif; ?>

                                        <figcaption class="article-recommended__title">
                                            <h3 class="h4">
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </h3>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- END Recommended -->
                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                <?php endif; ?>

                                <?php get_template_part('partials/ad-right-sidebar'); ?>
                            </div>
                        </div>
                    </aside>
                    <!-- END Article Sidebar -->
                </div>
                <!-- END post details -->

                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                <div class="article-single__category">Related Topics: <?php the_category( ', ' ); ?></div>
            </article>
            <!-- /article -->

            <?php
            $content_ad_listicle = get_field('content_ad_listicle', 'option');
            $content_ad_inner = get_field('content_ad_inner', 'option');

            if(get_field('select_type') == 'Listicle'):
                if($content_ad_listicle): ?>
                    <div class="adspace-leaderboard text-center">
                        <?php the_field('content_ad_listicle', 'option'); ?>
                    </div>
                    <!-- END Content Listicle Ad -->
                <?php 
                endif;
            else:
                if($content_ad_inner): ?>
                    <div class="adspace-leaderboard text-center">
                        <?php the_field('content_ad_inner', 'option'); ?>
                    </div>
                    <!-- END Content Inner Ad -->
                <?php 
                endif;
            endif; ?>

            <?php get_template_part('partials/related-articles'); ?>

            <?php comments_template(); ?>
        <?php endwhile; ?>

        <?php else: ?>

            <article>
                <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
            </article>
            <!-- /article -->

        <?php endif; ?>
        </section>
	</main>

<?php get_footer(); ?>