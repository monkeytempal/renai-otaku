<?php get_header(); ?>

	<main class="content">
		<!-- section -->
		<section class="container">
            <div class="row pt-3">
                <div class="col-md-8 ml-auto mr-auto">
                    <h1 class="section-title"><?php the_title(); ?></h1>

                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                            <?php the_content(); ?>

                        </article>
                        <!-- /article -->

                    <?php endwhile; ?>

                    <?php else: ?>

                        <article>

                            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>
                </div>
            </div>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
