(function( root, $, undefined ) {
	"use strict";

	$(function () {
		// DOM ready, take it away
		$('.sidebar-sticky').stickySidebar({
			topSpacing: 90,
			bottomSpacing: 60,
			containerSelector: '.article-single__content',
			innerWrapperSelector: '.sidebar-sticky__wrapper',
			minWidth: 992
		  });

		  $(".toc__list-url").click(function(e) {
			e.preventDefault();
			var position = $($(this).attr("href")).offset().top - 80;	
			$("body, html").animate({
				scrollTop: position
			} , 800 );
		});

		$('#initSearch').on('click', function(){
			console.log('nani nani');
			$(this).parents('#navbarSupportedContent').toggleClass("search-init");
		});
	});

} ( this, jQuery ));