<?php
// Primary Category tag
function getPrimaryCategory($categoryClassName){
    // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
    $category = get_the_category();
    $useCatLink = true;

    // If post has a category assigned.
    if ($category){
        $category_display = '';
        $category_link = '';
        if ( class_exists('WPSEO_Primary_Term') )
        {
            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );
            if (is_wp_error($term)) { 
                // Default to first category (not Yoast) if an error is returned
                $category_display = $category[0]->name;
                $category_link = get_category_link( $category[0]->term_id );
            } else { 
                // Yoast Primary category
                $category_display = $term->name;
                $category_link = get_category_link( $term->term_id );
            }
        } 
        else {
            // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_link = get_category_link( $category[0]->term_id );
        }

        // Display category
        if ( !empty($category_display) ){
            if ( $useCatLink == true && !empty($category_link) ){
            echo '<a href="'.$category_link.'" class="'.$categoryClassName.'">'.htmlspecialchars($category_display).'</a>';
            } else {
            echo '<span class="post-category '.$categoryClassName.'">'.htmlspecialchars($category_display).'</span>';
            }
        }
    }
}

// Custom article list
function articleList($type, $categoryName=''){
    $args = array(
        'posts_per_page' => 5,
        'ignore_sticky_posts' => true,
        'orderby' => $type,
        'category_name' => $categoryName,
        'order' => 'DESC'
    );

    $postlist = new WP_Query( $args );
    ?>

    <div class="article-list">

    <?php while ( $postlist->have_posts() ) : $postlist->the_post(); ?>

    <div class="article-list__item media">
      <a href="<?php the_permalink() ?>" class="article-list__thumb">
        <?php if( has_post_thumbnail() ):
          the_post_thumbnail('article_sm', array('class' => 'article-list__img'));
        else: ?>
          <img src="https://placehold.it/90x80" class="article-list__img" alt="<?php the_title();?>">
        <?php endif; ?>
      </a>

      <div class="media-body">
        
        <?php getPrimaryCategory('article-list__category'); ?>
        <h4 class="article-list__title"><a href="<?php the_permalink() ?>" title="<?php the_title();?>"><?php the_title();?></a></h4>
        <span class="article-list__date"><?php the_time('M j, Y'); ?></span>
      </div>
    </div>

    <?php endwhile; wp_reset_query(); ?>
    </div>
<?php
}

// Related Posts
if ( ! function_exists( 'att_related_posts' ) ) {
    function att_related_posts() {
        wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
        'no_found_rows'             => true,
        'update_post_meta_cache'    => false,
        'update_post_term_cache'    => false,
        'ignore_sticky_posts'       => 1,
        'orderby'                   => 'rand',
        'post__not_in'              => array($post->ID),
        'posts_per_page'            => 3
        );

        // Related by categories

        $cats = get_post_meta($post->ID, 'related-cat', true);

        if ( !$cats ) {
            $cats = wp_get_post_categories($post->ID, array('fields'=>'ids'));
            $args['category__in'] = $cats;
        } else {
            $args['cat'] = $cats;
        }

        // Related by tags
        // $tags = get_post_meta($post->ID, 'related-tag', true);

        // if ( !$tags ) {
        //     $tags = wp_get_post_tags($post->ID, array('fields'=>'ids'));
        //     $args['tag__in'] = $tags;
        // } else {
        //     $args['tag_slug__in'] = explode(',', $tags);
        // }
        // if ( !$tags ) { $break = true; }

        $query = !isset($break)?new WP_Query($args):new WP_Query;
        return $query;
    }
}

// Author Box
function authorBox($modifierClass){ ?>
    <div class="media author-box <?php echo $modifierClass; ?>">
        <?php $author_id = get_the_author_meta('ID'); ?>
        <?php if($author_avatar = get_avatar_url(get_the_author_meta( 'ID' ), 32)): ?>
        <img src="<?php echo $author_avatar; ?>" alt="<?php echo get_the_author_meta('display_name');?>" class="author-box__img mr-3">
        <?php else: ?>
        <img src="https://placehold.it/40x40" alt="<?php echo get_the_author_meta('display_name');?>" class="author-box__img mr-3">
        <?php endif; ?>
        <div class="media-body">
            <h4 class="author-box__name"><?php echo get_the_author_meta('display_name'); ?></h4>
            <p class="author-box__desc"><?php echo get_the_author_meta('description');?></p>

            <ul class="list-inline author-box__social mb-0">
                <?php if(get_the_author_meta('facebook')): ?>
                <li class="list-inline-item">
                    <a href="https://facebook.com/<?php echo get_the_author_meta('facebook');?>" class="author-box__social__item facebook" target="_blank"></a>
                </li>
                <?php endif; ?>
                <?php if(get_the_author_meta('twitter')): ?>
                <li class="list-inline-item">
                    <a href="https://twitter.com/<?php echo get_the_author_meta('twitter');?>" class="author-box__social__item twitter" target="_blank"></a>
                </li>
                <?php endif; ?>
                <?php if(get_field('author_instagram', 'user_'.$author_id)): ?>
                <li class="list-inline-item">
                    <a href="https://instagram.com/<?php echo get_field('author_instagram', 'user_'.$author_id);?>" class="author-box__social__item instagram" target="_blank"></a>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
<?php 
}
?>