<?php
function renai_cosplay()
{
    register_post_type('cosplay', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Cosplay', 'html5blank'), // Rename these to suit
            'singular_name' => __('Cosplay', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Cosplay', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Cosplay', 'html5blank'),
            'new_item' => __('New Cosplay', 'html5blank'),
            'view' => __('View Cosplay', 'html5blank'),
            'view_item' => __('View Cosplay', 'html5blank'),
            'search_items' => __('Search Cosplay', 'html5blank'),
            'not_found' => __('No Cosplay found', 'html5blank'),
            'not_found_in_trash' => __('No Cosplay found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true,
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'comments'
        ),
        'can_export' => true,
        'menu_icon' => 'dashicons-format-gallery',
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

?>