<?php

/**
 * Image Box Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'img-box-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'img-box';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>
<?php
$image = get_field('img_box_image');
$image_align = get_field('img_box_align');

if($image){
    // Image variables.
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];

    // Thumbnail size attributes.
    $size = 'listicle_thumb';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];
}

// Conditionals to change alignment
if( $image_align == 'align_left' ) {
    $align_class = 'img-box__align--left';
} else if( $image_align == 'align_right' ) {
    $align_class = 'img-box__align--right';
}else {
    $align_class = null;
}

$allowed_blocks = array( 'core/heading', 'core/paragraph', 'core/list', 'core/quote' );
$template = array(
    array( 'core/heading', array(
        'level' => 4,
        'placeholder' => 'Heading Text',
        'className' => 'img-box__title',
    ) ),
    array( 'core/paragraph', array(
        'placeholder' => 'Add a paragraph'
    ) ),
);
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="img-box__wrapper <?php echo $align_class; ?>">
        <figure class="img-box__img">
            <?php if($image): ?>
            <img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />
            <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="Renai Otaku">
            <?php endif;
            if( $caption ): ?>
            <figcaption class="content-img__caption"><?php echo esc_html($caption); ?></figcaption>
            <?php endif; ?>
        </figure>
        <div class="img-box__content">
            <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" allowedBlocks="'.esc_attr( wp_json_encode( $allowed_blocks ) ).'" />'; ?>
        </div>
    </div>
</div>