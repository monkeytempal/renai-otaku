<?php

/**
 * Listicle Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'listicle-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'list-item';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
<?php
$allowed_blocks = array( 'core/heading', 'core/image', 'core/paragraph', 'core/list', 'core/quote' );
$template = array(
    array( 'core/heading', array(
        'placeholder' => 'Heading Text eg: 10. Awesome anime/manga title',
        'className' => 'list-item__title',
    ) ),
    array( 'core/image', array(
        'className' => 'list-item__image'
    ) ),
    array( 'core/paragraph', array(
        'placeholder' => 'Add a paragraph'
    ) ),
);
echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" allowedBlocks="'.esc_attr( wp_json_encode( $allowed_blocks ) ).'" />';
?>
</div>