<?php if(have_rows('deals', 'option')): ?>
<?php $i=0; ?>
<section class="deals-section">
    <div class="container">
        <h4 class="section-title section-title--centered">Hot Deals</h4>
        <div class="row">
            <?php while(have_rows('deals', 'option')): the_row(); 
                $image = get_sub_field('deals_thumbnail');
                $title = get_sub_field('deals_title');
                $link = get_sub_field('deals_link');
                $discount = get_sub_field('deals_discount');
                $price = get_sub_field('deals_price');
                $expiration = get_sub_field('deals_expire');
                
                // $deals_limit = the_field('deals_limit', 'option');

                $i++;
                // Limit Deals
                if( $i > 6 ){ break; }
            ?>
            <div class="col-md-4">
                <a href="<?php echo $link; ?>" class="deals__wrapper lazyload" rel="nofollow" data-bg="<?php echo $image['sizes']['video_thumb']; ?>">
                    <div class="deals__content">
                        <h4 class="deals__title"><?php echo $title; ?></h4>
                        <span class="deals__range"><?php echo $discount; ?></span>
                        <span class="deals__price"><?php echo $price; ?></span>
                        <span class="deals__expiration">* Deal ends on <?php echo $expiration; ?></span>
                    </div>
                </a>
            </div>
            <?php endwhile; ?>
        </div>
        <a href="<?php the_field('deals_more_url', 'option')?>" class="deals-section__link">See more deals</a>
    </div>
</section>
<!-- END Deals -->
<?php endif; ?>