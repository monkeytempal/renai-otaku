<div class="featured-reviews">
    <div class="row article-list">
    <?php
    $args = array('posts_per_page' => 6, 'category_name' => 'review', 'order' => 'DESC');
    $popularpost = new WP_Query($args);

    while ($popularpost->have_posts()) : $popularpost->the_post(); ?>
    <?php $review_count = get_field('review_ratings'); ?>    
    <?php if ($popularpost->current_post == 0 || $popularpost->current_post == 1 || $popularpost->current_post == 2) : ?>
        <div class="col-md-4 featured-reviews__item">
            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <div class="article-list__item--featured lazyload" data-bg="<?php the_post_thumbnail_url('featured_list');?>">
            <?php else: ?>
            <div class="article-list__item--featured lazyload" data-bg="<?php echo get_template_directory_uri(); ?>/img/placeholder.png">
            <?php endif; ?>
                <div class="article-list__block">
                    <?php if($review_count): $rating_value = $review_count*100/5; ?>
                    <div class="article-list__ratings ratings">
                        <span class="ratings__star stars-outer">
                            <span class="stars-inner" style="width: <?php echo $rating_value; ?>%;"></span>
                        </span>
                    </div>
                    <?php endif; ?>
                    
                    <h4 class="article-list__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <ul class="article-list__author">
                        <li>
                        <?php if($author_avatar = get_avatar_url(get_the_author_meta( 'ID' ), 32)): ?>
                        <img src="<?php echo $author_avatar; ?>" alt="<?php echo get_the_author_meta('display_name');?>" class="article-list__author__img">
                        <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php echo get_the_author_meta('display_name');?>" class="article-list__author__img">
                        <?php endif; ?>
                            <span class="article-list__author__name"><?php the_author_posts_link(); ?></span>
                        </li>
                        <li>
                            <span class="article-list__author__date"><?php the_date('M, j, Y'); ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END Featured Review item -->

        <?php else : ?>
        <div class="col-md-4 col-sm-6 featured-reviews__item">
            <div class="article-list__item media">
                <figure class="article-list__thumbnail">
                <?php if ( has_post_thumbnail() ) :?>
                    <?php the_post_thumbnail('article_sm', array('class' => 'article-list__img'));?>
                <?php else: ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" class="article-list__img" alt="">
                <?php endif; ?>
                <?php if($review_count): ?>
                    <span class="article-list__ratings--number"><?php echo $review_count; ?></span>
                <?php endif; ?>
                </figure>
                <div class="media-body">
                    <?php getPrimaryCategory('article-list__category'); ?>
                    <h4 class="article-list__title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h4>
                </div>
            </div>
        </div>
        <!-- END Featured Review item -->
        <?php endif; ?>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</div>
<!-- END Featured Reviews -->