<?php
  $leaderboard_ad = get_field('leaderboard_ad', 'option');
  if($leaderboard_ad): ?>
  <div class="adspace-leaderboard">
    <?php echo $leaderboard_ad; ?>
  </div>
  <!--END Leaderboard Ad -->
<?php endif; ?>
