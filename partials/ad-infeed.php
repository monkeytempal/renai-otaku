<?php
$article_infeed_ad = get_field('article_infeed_ad', 'option');
if($article_infeed_ad): ?>
<div class="adspace-leaderboard">
    <?php echo $article_infeed_ad; ?>
</div>
<?php endif; ?>