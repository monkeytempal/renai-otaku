<div class="recent-articles">
    <?php 
    $args = array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 5, 'order' => 'DESC');
    $recent_posts = new WP_Query( $args );?>

    <?php if ($recent_posts->have_posts()): while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
    <article class="recent-articles__item <?php echo (get_field('review_ratings') ? 'recent-articles__item--review' : ''); ?>">
        <figure class="recent-articles__thumbnail">
        <?php if ( has_post_thumbnail() ) :?>
            <?php the_post_thumbnail('article', array('class' => 'recent-articles__img'));?>
        <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="" class="recent-articles__img">
        <?php endif; ?>

        <?php $review_count = get_field('review_ratings'); ?>
        <?php if($review_count): $rating_value = $review_count*100/5; ?>
            <div class="recent-articles__ratings ratings">
                <span class="ratings__star stars-outer">
                    <span class="stars-inner" style="width: <?php echo $rating_value; ?>%;"></span>
                </span>
                <span class="ratings__number"><?php echo $review_count;?></span>
            </div>
        <?php endif; ?>
        </figure>
        <div class="recent-articles__content">
            <span class="recent-articles__category"><?php getPrimaryCategory('text-danger'); ?> By <?php the_author_posts_link(); ?></span>
            <h2 class="recent-articles__title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h2>
            <p><?php echo wp_trim_words( get_the_excerpt(), 50, '...' ) ?></p>
        </div>
    </article>
    <!-- END Recent Article item -->
    <?php endwhile; endif; ?>

    <a href="<?php echo home_url(); ?>/recent" class="btn btn-lg btn-yellow btn--more">See More</a>
</div>