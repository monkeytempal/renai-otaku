<section class="article-related">
	<?php $related = att_related_posts(); ?>

	<?php if ( $related->have_posts() ): ?>
	<h4 class="article-related__heading">
		<?php _e('You may also like...','html5blank'); ?>
	</h4>

	<div class="row">
		<?php while ( $related->have_posts() ) : $related->the_post(); ?>
      <div class="col-sm-4 article-related__item">
        <article <?php post_class(); ?>>
          <figure class="article-related__thumbnail">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
              <?php if ( has_post_thumbnail() ): ?>
                <?php the_post_thumbnail('featured', array('class'=>'article-related__thumbnail__img')); ?>
              <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php the_title(); ?>" class="article-related__thumbnail__img" />
              <?php endif; ?>
            </a>
          </figure>
          <!-- END Post thumbnail-->

          <div class="article-related__content">
            <h4 class="article-related__title">
              <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h4>
            <!-- END Post title-->
          </div>
          <!-- END Related-inner-->
        </article>
      </div>
      <!-- END Column -->
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div><!-- END Row -->
	<?php endif; ?>

	<?php wp_reset_query(); ?>
</section>
<!-- END Related Posts -->
