<div class="article-cosplay__intro article-cosplay__block mb-4">
    <?php the_field('cosplay_introduction'); ?>
</div>

<div class="cosplay-gallery">
<?php
    // check if the repeater field has rows of data
    if( have_rows('cosplay_gallery') ):

    // loop through the rows of data
        while ( have_rows('cosplay_gallery') ) : the_row();
    ?>

        <?php
        //display a sub field value
        $cosplay_photo = get_sub_field('cosplay_photo');
        $cosplayer_name = get_sub_field('cosplay_cosplayer');
        $cosplayer_link = get_sub_field('cosplay_cosplayer_link');
        $photographer_name = get_sub_field('cosplay_photographer');
        $photographer_link = get_sub_field('cosplay_photographer_link');
        $cosplay_details = get_sub_field('cosplay_details');
        
        // thumbnail
        $alt = $cosplay_photo['alt'];
        $size = 'featured_thumb';
        $thumb = $cosplay_photo['sizes'][ $size ];
        ?>

        <figure class="cosplay-gallery__figure">
            <?php if( !empty($cosplay_photo) ): ?>
            <img src="<?php echo esc_url($cosplay_photo['url']); ?> ?>" alt="<?php echo $alt; ?>" class="cosplay-gallery__img" />
            <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="Cosplay" class="cosplay-gallery__img" />
            <?php endif; ?>

            <figcaption class="cosplay-gallery__caption">
                <span class="cosplay-gallery__caption__item">Cosplayer: <a href="<?php echo $cosplayer_link; ?>"><?php echo $cosplayer_name; ?></a></span>
                <span class="cosplay-gallery__caption__item">Photographer: <a href="<?php echo $photographer_link; ?>"><?php echo $photographer_name; ?></a></span>
            </figcaption>
        </figure>

        <?php if($cosplay_details): ?><div class="cosplay-gallery__details article-cosplay__block"><?php echo $cosplay_details; ?></div><?php endif; ?>
    <?php
        endwhile;
    else :

    // no rows found
    echo "No items to display";

    endif;
    ?>
</div>

<div class="article-cosplay__block">
    <?php the_field('cosplay_conclusion'); ?>
</div>