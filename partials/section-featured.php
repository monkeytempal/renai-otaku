<div class="featured-articles article-list row">
    <div class="col-md-7">
    <?php
        $args = array('posts_per_page' => 1, 'order' => 'DESC', 'post_type' => 'cosplay');
        $latestPost = new WP_Query($args);

        while ($latestPost->have_posts()) : $latestPost->the_post(); ?>
        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
        <div class="article-list__item--featured lazyload" data-bg="<?php the_post_thumbnail_url('featured_list');?>">
        <?php else: ?>
        <div class="article-list__item--featured lazyload" data-bg="<?php echo get_template_directory_uri(); ?>/img/placeholder.png">
        <?php endif; ?>
            <?php getPrimaryCategory('article-list__category'); ?>
            <div class="article-list__block">
                <h1 class="article-list__title">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h1>
                
                <?php $review_count = get_field('review_ratings'); ?>
                <?php if($review_count): $rating_value = $review_count*100/5; ?>
                <div class="article-list__ratings ratings">
                    <span class="ratings__star stars-outer">
                        <span class="stars-inner" style="width: <?php echo $rating_value; ?>%;"></span>
                    </span>
                </div>
                <?php endif; ?>

                <ul class="article-list__author">
                    <li>
                        <?php if($author_avatar = get_avatar_url(get_the_author_meta( 'ID' ), 32)): ?>
                        <img src="<?php echo $author_avatar; ?>" alt="<?php echo get_the_author_meta('display_name');?>" class="article-list__author__img">
                        <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php echo get_the_author_meta('display_name');?>" class="article-list__author__img">
                        <?php endif; ?>
                        <span class="article-list__author__name"><?php the_author_posts_link(); ?></span>
                    </li>
                    <li>
                        <span class="article-list__author__date"><?php the_date(); ?></span>
                    </li>
                </ul>
            </div>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <!-- END Featured Article -->

    <div class="col-md-5">
        <?php
        $args = array('posts_per_page' => 4, 'orderby' => 'rand', 'order' => 'DESC');
        $popularpost = new WP_Query($args);
        while ($popularpost->have_posts()) : $popularpost->the_post(); ?>
        <div class="article-list__item media">
            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <?php the_post_thumbnail('article_sm', array('class' => 'article-list__img')); // Declare pixel size you need inside the array ?>
            <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php the_title(); ?> - Renai Otaku" class="article-list__img" />
            <?php endif; ?>
            <div class="media-body">
                <?php getPrimaryCategory('article-list__category'); ?>
                <h4 class="article-list__title">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h4>
                <span class="article-list__date"><?php the_date('M, j, Y'); ?></span>
            </div>
        </div>
        <!-- END Article item -->
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
</div>
<!-- END Featured Articles -->