<div class="article-single__content__intro mb-4">
    <?php the_field('listicle_introduction'); ?>
</div>

<div class="toc">
    <h4 class="toc__title">On this page:</h4>
    <ul class="toc__list">
    <?php if( have_rows('list_items') ): while ( have_rows('list_items') ) : the_row(); 
        $title = get_sub_field('title');
        $title_id = preg_replace("/[^a-zA-Z0-9]+/", "", $title);
        ?>
        <li class="toc__list-item">
            <a href="#<?php echo $title_id; ?>" class="toc__list-url"><?php echo $title; ?></a>
        </li>
    <?php endwhile; endif; ?>
    </ul>
</div>

<div class="list-item__wrapper">
<?php
    // check if the repeater field has rows of data
    if( have_rows('list_items') ):

    // loop through the rows of data
        while ( have_rows('list_items') ) : the_row();
    ?>

        <?php
        //display a sub field value
        $title = get_sub_field('title');
        $image = get_sub_field('image');
        $alt = $image['alt'];
        $description = get_sub_field('description');
        $title_id = preg_replace("/[^a-zA-Z0-9]+/", "", $title);

        // thumbnail
        $size = 'featured_list';
        $thumb = $image['sizes'][ $size ];
        ?>

    <div class="list-item">
        <h2 class="h4 list-item__title" id="<?php echo $title_id; ?>"><?php echo $title; ?></h2>
        <figure class="list-item__thumbnail">
            <?php if( !empty($image) ): ?>
            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" class="list-item__image" />
            <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="Top ten anime" class="list-item__image" />
            <?php endif; ?>
        </figure>

        <div class="list-item__content">
            <?php echo $description; ?>
        </div>
    </div>
    <?php
        endwhile;
    else :

    // no rows found
    echo "No items to display";

    endif;
    ?>
</div>

<h3 class="title-styled mb-1 mt-3">Conclusion</h3>
<?php the_field('listicle_conclusion'); ?>