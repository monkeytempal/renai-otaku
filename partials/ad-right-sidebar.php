<?php $sidebar_ad_right = get_field('sidebar_ad_right', 'option'); ?>
<?php if($sidebar_ad_right): ?>
<div class="sidebar-widget adspace-sidebar">
    <?php the_field('sidebar_ad_right', 'option'); ?>
</div>
<!--END Advertisement Box-->
<?php endif; ?>