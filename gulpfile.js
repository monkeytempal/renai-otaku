const gulp = require('gulp');
const sass = require('gulp-sass');
const combineMq = require('gulp-combine-mq')
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();

const config   = require('./gulpconfig.json');

// Compile Scss
function compileStyles(){
    return gulp.src('./app/scss/**/*.scss')
    .pipe(plumber())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./app/dist'))
    .pipe(browserSync.stream());
}

function cleanStyles(){
    return gulp.src('./app/dist/*.css')
    .pipe(combineMq())
    .pipe(gulp.dest('./css'));
}

function vendorCSS(){
    return gulp.src(config.path.vendorCSS)
    .pipe(concat('vendor.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./css'));
}

function vendorJS(){
    return gulp.src(config.path.vendorJS)
    .pipe(concat('vendor.min.js'))
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest('./js'))
}

function watch(){
    browserSync.init({
        proxy: "http://localhost/renaiotaku.com/"
    });
    gulp.watch('./app/scss/**/*.scss', compileStyles);
    gulp.watch('./app/dist/*.css', cleanStyles);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./*.js').on('change', browserSync.reload);
}

exports.compileStyles = compileStyles;
exports.cleanStyles = cleanStyles;
exports.vendorCSS = vendorCSS;
exports.vendorJS = vendorJS;
exports.watch = watch;