<?php
    get_header();

    // $review_count = get_field('review_ratings');
	// $review_count = ($review_count ? $review_count : 0);
    // $rating_value = $review_count*100/5;
?>

	<main class="content">
        <section class="article-single article-cosplay">
        <?php //get_template_part('partials/ad-leaderboard'); ?>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <?php if(get_field('cosplay_choice') == 'gallery'): ?>
            <div class="article-single__banner" style="background-image: url(<?php the_post_thumbnail_url(); ?>); ">
                <div class="article-single__banner__content">
                    <span class="article-single__category article-single__category--primary"><?php getPrimaryCategory('article-list__category'); ?></span>
                    <h1 class="article-single__title"><?php the_title(); ?></h1>
                </div>
            </div>
            <!-- END post thumbnail -->
            <?php endif; ?>

            <div class="container">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if(get_field('cosplay_choice') == 'article'): ?>
                    <header class="article-single__header">
                        <span class="article-single__category article-single__category--primary"><?php getPrimaryCategory('article-list__category'); ?></span>
                        <h1 class="article-single__title"><?php the_title(); ?></h1>
                    </header>

                    <figure class="article-single__thumb">
                        <?php if ( has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail(); ?>
                        <?php endif; ?>
                    </figure>
                    <!-- END post thumbnail -->
                    <?php endif; ?>

                    <div class="row justify-content-center article-single__content">
                        <div class="article-single__text col-lg-10">
                            <div class="article-single__meta article-cosplay__block">
                                <div class="article-single__author">
                                    <?php if($author_avatar = get_avatar_url(get_the_author_meta( 'ID' ), 32)): ?>
                                    <img src="<?php echo $author_avatar; ?>" alt="<?php echo get_the_author_meta('display_name');?>" class="article-single__author__img">
                                    <?php else: ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="<?php echo get_the_author_meta('display_name');?>" class="article-single__author__img">
                                    <?php endif; ?>
                                    <p class="article-single__author__name">By <strong><?php the_author_posts_link(); ?></strong></p>
                                    <p class="article-single__author__date">Published on <strong><?php the_time('F j, Y'); ?></strong></p>
                                </div>
                                <!-- END Author -->

                                <ul class="article-single__social list-inline">
                                    <li class="list-inline-item">
                                        <button type="button" class="article-single__social__link" data-sharer="facebook" data-url="<?php the_permalink(); ?>"><span class="icon icon-facebook"></span></button>
                                    </li>
                                    <li class="list-inline-item">
                                        <button type="button" class="article-single__social__link" data-sharer="twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>" data-hashtags="renaiotaku"><span class="icon icon-twitter"></span></button>
                                    </li>
                                    <li class="list-inline-item">
                                        <button type="button" class="article-single__social__link" data-sharer="reddit" data-url="<?php the_permalink(); ?>"><span class="icon icon-reddit"></span></button>
                                    </li>
                                    <li class="list-inline-item">
                                        <button type="button" class="article-single__social__link" data-sharer="tumblr" data-caption="Check out this awesome article" data-title="<?php the_title(); ?>" data-tags="<?php $tags = get_the_tags(); foreach($tags as $tag) {echo "$tag->name, "; } ?>" data-url="<?php the_permalink(); ?>"><span class="icon icon-tumblr"></span></button>
                                    </li>
                                </ul>
                            </div>
                            <!-- END Meta -->

                            <?php
                            the_content(); // Dynamic content

                            if(get_field('cosplay_choice') === 'article') {
                                echo '<div class="article-cosplay__block">'.get_field('cosplay_article').'</div>';
                            }else {
                                get_template_part('partials/content-cosplay');
                            }
                                                 
                            $content_bottom_ad = get_field('content_bottom_ad');
                            
                            if($content_bottom_ad): ?>
                            <div class="adspace-leaderboard text-center">
                                <?php echo $content_bottom_ad ?>
                            </div>
                            <!-- END Content top Ad -->

                            <?php endif; ?>
                            <div class="article-cosplay__block">
                                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                                <div class="article-single__category">Related Topics: <?php the_category( ', ' ); ?></div>
                            </div>
                        </div>
                        <!-- END Article Content -->
                    </div>
                    <!-- END post details -->
                </article>
                <!-- /article -->

                <div class="article-cosplay__block">
                    <?php
                    $content_ad_listicle = get_field('content_ad_listicle', 'option');
                    $content_ad_inner = get_field('content_ad_inner', 'option');

                    if(get_field('select_type') == 'Listicle'):
                        if($content_ad_listicle): ?>
                            <div class="adspace-leaderboard text-center">
                                <?php the_field('content_ad_listicle', 'option'); ?>
                            </div>
                            <!-- END Content Listicle Ad -->
                        <?php 
                        endif;
                    else:
                        if($content_ad_inner): ?>
                            <div class="adspace-leaderboard text-center">
                                <?php the_field('content_ad_inner', 'option'); ?>
                            </div>
                            <!-- END Content Inner Ad -->
                        <?php 
                        endif;
                    endif; ?>

                    <?php get_template_part('partials/related-articles'); ?>

                    <?php comments_template(); ?>
                </div>
            <?php endwhile; ?>

            <?php else: ?>

                <article>
                    <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
                </article>
                <!-- /article -->
            </div>
        <?php endif; ?>
        </section>
	</main>

<?php get_footer(); ?>