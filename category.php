<?php get_header(); ?>

	<main class="content">
		<section class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="section-title"><?php single_cat_title(); ?></h1>
                    <?php get_template_part('partials/ad-infeed'); ?>
                    <?php get_template_part('loop'); ?>
                    <?php get_template_part('pagination'); ?>
                    <?php get_template_part('partials/ad-leaderboard'); ?>
                </div>
                
                <?php get_sidebar(); ?>
            </div>
		</section>
        <!-- END section -->
        
        <?php get_template_part('partials/section-deals'); ?>
	</main>

<?php get_footer(); ?>
