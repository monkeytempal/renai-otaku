<?php get_header(); ?>

    <main class="content">
		<section class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="section-title"><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
                    <?php get_template_part('partials/ad-infeed'); ?>
                    <?php get_template_part('loop'); ?>
                    <?php get_template_part('pagination'); ?>
                    <?php get_template_part('partials/ad-leaderboard'); ?>
                </div>
                
                <?php get_sidebar(); ?>
            </div>
		</section>
        <!-- END section -->
	</main>

<?php get_footer(); ?>
