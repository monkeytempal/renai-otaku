        <footer class="footer">
            <div class="container">
                <ul class="list-inline footer-social">
                    <li class="list-inline-item">
                        <a href="https://facebook.com/<?php the_field('facebook_url', 'option'); ?>" class="footer-social__link facebook" target="_blank"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://twitter.com/<?php the_field('twitter_url', 'option'); ?>" class="footer-social__link twitter" target="_blank"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://instagram.com/<?php the_field('ig_url', 'option'); ?>" class="footer-social__link instagram" target="_blank"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://youtube.com/<?php the_field('youtube_url', 'option'); ?>" class="footer-social__link youtube" target="_blank"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://<?php the_field('tumblr_url', 'option'); ?>.tumblr.com/" class="footer-social__link tumblr" target="_blank"></a>
                    </li>
                </ul>
                <!-- END Footer Social -->

                <?php if(have_rows('footer_links', 'option')): ?>
                <ul class="list-inline footer-links">
                    <?php while(have_rows('footer_links', 'option')): the_row(); 
                        $title = get_sub_field('url_title');
                        $link = get_sub_field('url_link');
                    ?>
                    <li class="list-inline-item"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></li>
                    <?php endwhile; ?>
                </ul>
                <?php endif; ?>
            </div>

            <div class="footer-copyright">
                <p class="mb-0">&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. | <a href="/privacy-policy">Privacy Policy</a></p>
            </div>
        </footer>
    </div>
    <!-- END Site Wrapper -->

	<?php wp_footer(); ?>
    <?php the_field('analytics_code', 'option'); ?>
	</body>
</html>
