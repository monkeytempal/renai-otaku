<aside class="sidebar col-lg-3">
    <div class="sidebar-widget sidebar__social">
        <h4 class="sidebar__title">Follow us on</h4>
        <ul class="list-inline sidebar__social__list">
            <li class="list-inline-item">
                <a href="https://facebook.com/<?php the_field('facebook_url', 'option'); ?>" class="sidebar__social__item facebook" target="_blank"></a>
            </li>
            <li class="list-inline-item">
                <a href="https://twitter.com/<?php the_field('twitter_url', 'option'); ?>" class="sidebar__social__item twitter" target="_blank"></a>
            </li>
            <li class="list-inline-item">
                <a href="https://instagram.com/<?php the_field('ig_url', 'option'); ?>" class="sidebar__social__item instagram" target="_blank"></a>
            </li>
            <li class="list-inline-item">
                <a href="https://youtube.com/<?php the_field('youtube_url', 'option'); ?>" class="sidebar__social__item youtube" target="_blank"></a>
            </li>
            <li class="list-inline-item">
                <a href="https://<?php the_field('tumblr_url', 'option'); ?>.tumblr.com/" class="sidebar__social__item tumblr" target="_blank"></a>
            </li>
        </ul>
    </div>
    <!-- END Social Sidebar -->
	
    <?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-widget' ) ) ?>

    <div class="sidebar-widget">
        <ul class="nav nav-tabs" id="sidebarPopularTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#popular" role="tab">Popular</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#recent" role="tab">Recent</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="popular" role="tabpanel">
                <?php articleList("rand"); ?>
            </div>
            <!-- END Popular Tab -->
            <div class="tab-pane fade" id="recent" role="tabpanel">
                <?php articleList("date"); ?>
            </div>
            <!-- END Recent Tab -->
        </div>
    </div>
    <!-- END Tabbed Sidebar Widget -->

    <?php if(!is_front_page()): ?>
    <?php get_template_part('partials/ad-right-sidebar'); ?>
    <?php endif; ?>
</aside>