<?php get_header(); ?>

	<main class="content">
		<section class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="section-title"><?php _e( 'Recent Articles', 'html5blank' ); ?></h1>
                    <?php get_template_part('partials/ad-leaderboard'); ?>
                    <?php get_template_part('loop'); ?>
                    <?php get_template_part('pagination'); ?>
                </div>
                
                <?php get_sidebar(); ?>
            </div>
		</section>
        <!-- END section -->
        
        <?php get_template_part('partials/section-deals'); ?>
	</main>

<?php get_footer(); ?>
