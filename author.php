<?php acf_form_head(); get_header(); ?>

	<main class="content mb-5">
		<section class="container">
            <?php if (have_posts()): the_post(); ?>
                <?php authorBox("author-box--single"); ?>
                <!-- END Author box -->
                
                <h1 class="section-title"><?php _e( 'Articles by ', 'html5blank' ); echo get_the_author(); ?></h1>
                
                <?php //get_template_part('partials/ad-infeed'); ?>

                <div class="row">
                <?php rewind_posts(); while (have_posts()) : the_post(); ?>
                    <article class="col-md-6 col-lg-4 author-article__item">
                        <figure class="author-article__thumbnail">
                            <?php if ( has_post_thumbnail() ) :?>
                                <?php the_post_thumbnail('video_thumb', array('class' => 'author-article__img'));?>
                            <?php else: ?>
                                <img src="https://placehold.it/200x250" alt="" class="author-article__img">
                            <?php endif; ?>
                            
                            <?php $review_count = get_field('review_ratings'); ?>
                            <?php if($review_count): $rating_value = $review_count*100/5; ?>
                                <div class="author-article__ratings ratings">
                                    <span class="ratings__star stars-outer">
                                        <span class="stars-inner" style="width: <?php echo $rating_value; ?>%;"></span>
                                    </span>
                                    <!-- <span class="ratings__number"><?php echo $review_count;?></span> -->
                                </div>
                            <?php endif; ?>
                        </figure>
                        
                        <div class="author-article__content">
                            <span class="author-article__category"><?php getPrimaryCategory(''); ?></span>
                            <h2 class="author-article__title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p><?php echo wp_trim_words( get_the_excerpt(), 40, '...' ) ?></p>
                        </div>
                    </article>
                    <!-- /article -->
                <?php endwhile; else: ?>
                    <!-- article -->
                    <article>

                        <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                    </article>
                    <!-- /article -->
                <?php endif; ?>
                </div>

            <?php get_template_part('pagination'); ?>

            <?php get_template_part('partials/ad-leaderboard'); ?>
		</section>
        <!-- END Section -->
	</main>

<?php get_footer(); ?>
