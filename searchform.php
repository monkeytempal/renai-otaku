<!-- search -->
<form class="search-form" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-form__input" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
	<button class="search-form__submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>
<!-- /search -->
