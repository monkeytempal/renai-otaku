<?php /* Template Name: Deals Page Template */ get_header(); ?>

	<main class="content">
		<section class="container">
			<h1 class="section-title"><?php the_title(); ?></h1>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
                <?php if(have_rows('deals', 'option')): ?>
                    <div class="row">
                        <?php while(have_rows('deals', 'option')): the_row(); 
                            $image = get_sub_field('deals_thumbnail');
                            $title = get_sub_field('deals_title');
                            $link = get_sub_field('deals_link');
                            $discount = get_sub_field('deals_discount');
                            $price = get_sub_field('deals_price');
                            $expiration = get_sub_field('deals_expire');
                        ?>
                        <div class="col-md-4">
                            <a href="<?php echo $link; ?>" class="deals__wrapper" style="background-image: url(<?php echo $image['url']; ?>);">
                                <div class="deals__content">
                                    <h4 class="deals__title"><?php echo $title; ?></h4>
                                    <span class="deals__range"><?php echo $discount; ?></span>
                                    <span class="deals__price"><?php echo $price; ?></span>
                                    <span class="deals__expiration">* Deal ends on <?php echo $expiration; ?></span>
                                </div>
                            </a>
                        </div>
                        <?php endwhile; ?>
                    </div>
                <!-- END Deals -->
                <?php endif; ?>
			</article>
			<!-- /article -->
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
