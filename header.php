<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php wp_head(); ?>
        <?php the_field('wp_head', 'option'); ?>

        <link href="https://fonts.googleapis.com/css?family=Asap:400,400i,600,600i|Lato:700,700i,900,900i&display=swap" rel="stylesheet">
	</head>
	<body <?php body_class(); ?>>

    <div class="site-wrapper">
        <header class="header">
            <nav class="fixed-top navbar navbar-expand-md navbar-dark bg-dark">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Renai Otaku"></a>
                    <!-- END Logo -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php header_nav(); ?>
                        <button class="search-btn" type="button" id="initSearch">
                            <span class="icon"></span>
                        </button>

                        <?php get_template_part( 'searchform' ); ?>
                    </div>
                </div>
            </nav>
        </header>
        <!-- END Header -->
