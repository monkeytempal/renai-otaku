<?php get_header(); ?>

	<main class="content">
		<section class="container">
			<article id="post-404" class="pageNotFound">
                <img src="<?php echo get_template_directory_uri(); ?>/img/chibi.png" alt="Renai Otaku" class="pageNotFound__img">
				<h1 class="pageNotFound__title"><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<a href="<?php echo home_url(); ?>" class="pageNotFound__link"><?php _e( 'Return home?', 'html5blank' ); ?></a>
			</article>
		</section>
	</main>

<?php get_footer(); ?>
