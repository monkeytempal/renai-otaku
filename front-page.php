<?php get_header(); ?>

<main class="content">
    <section class="featured-section container">
        <div class="row">
            <div class="col-lg-9">
                <?php get_template_part('partials/section-featured'); ?>

                <?php get_template_part('partials/ad-leaderboard'); ?>

                <?php get_template_part('partials/section-featured-reviews'); ?>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </section>
    <!-- END Featured Section -->

    <?php if(have_rows('homepage_cta_buttons', 'option')): ?>
    <section class="cta-buttons container">
        <div class="row">
        <?php while(have_rows('homepage_cta_buttons', 'option')): the_row(); 
            $image = get_sub_field('button_image');
            $text = get_sub_field('button_text');
            $link = get_sub_field('button_link');
        ?>
            <div class="col-sm-6 col-lg-3 cta-buttons__item">
                <a href="<?php echo $link; ?>" class="cta-buttons__link lazyload" data-bg="<?php echo $image['url']; ?>">
                    <span><?php echo $text ?></span>
                </a>
            </div>
        <?php endwhile; ?>
        </div>
    </section>
    <!-- END Cta Buttons -->
    <?php endif; ?>

    <section class="articles-section container">
        <div class="row">
            <div class="col-lg-9">
                <?php get_template_part('partials/ad-infeed'); ?>

                <?php get_template_part('partials/section-recent-articles'); ?>

                <?php
                $article_infeed_ad_bottom = get_field('article_infeed_ad_bottom', 'option');
                if($article_infeed_ad_bottom): ?>
                <div class="adspace-leaderboard">
                    <?php echo $article_infeed_ad_bottom; ?>
                </div>
                <?php endif; ?>
            </div>
            <aside class="col-lg-3 sidebar">
                <div class="sidebar-widget">
                    <h4 class="sidebar__title">Recent Top 10s</h4>
                    <?php articleList("date", "top-tens"); ?>
                </div>
                <!-- END Sidebar Widget -->

                <?php get_template_part('partials/ad-right-sidebar'); ?>

                <div class="sidebar-widget">
                    <h4 class="sidebar__title">Recent Comments</h4>
                    <div class="article-list article-list--comments">
                    <?php $args = array('status' => 'approve', 'number' => '4', 'orderby' => 'comment_date', 'order' => 'DESC', 'post_status'=>'publish'); ?>
                    <?php $comments = get_comments($args);
                        foreach($comments as $comment) :?>
                        <div class="article-list__item media">
                            <?php echo get_the_post_thumbnail($comment->comment_post_ID, 'article_sm', array('class' => 'article-list__img')); ?>
                            <div class="media-body">
                                <p class="mb-0"><strong class="author"><?php echo ($comment->comment_author); ?></strong> says <span class="content"><?php echo wp_trim_words($comment->comment_content, 10) ?></span> on <a href="<?php echo get_comment_link($comment->comment_ID); ?>"><?php echo get_the_title($comment->comment_post_ID); ?></a></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- END Sidebar Widget -->
            </aside>
        </div>
    </section>
    <!-- Recent Articles Section -->

    <?php if(have_rows('youtube_video', 'option')): ?>
    <section class="videos-section container mb-4">
        <h4 class="section-title">Recent Videos</h4>
        <div class="row">
            <?php while(have_rows('youtube_video', 'option')): the_row(); 
                $image = get_sub_field('video_thumbnail');
                $title = get_sub_field('video_title');
                $link = get_sub_field('video_link');
            ?>
            <div class="col-md-6 col-lg-4">
                <a href="<?php echo $link; ?>" class="video-cta lazyload" data-bg="<?php echo $image['sizes']['video_thumb']; ?>">
                    <h3 class="video-cta__title"><?php echo $title; ?></h3>
                </a>
            </div>
            <?php endwhile; ?>
        </div>
    </section>
    <!-- END Videos Section -->
    <?php endif; ?>

    <?php get_template_part('partials/section-deals'); ?>
</main>
<!-- END Main Content -->

<?php get_footer(); ?>